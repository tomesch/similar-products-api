const request = require("supertest-promised");
const app = require("../../../api");

test("returns a list of similarly colored products", done => {
  request(app)
    .get("/similar-color-products/PH7111-00-001")
    .then(res => {
      expect(res.text).toMatchSnapshot();
      done();
    });
});

test("takes the nbOfProducts query parameter into account", done => {
  request(app)
    .get("/similar-color-products/PH7111-00-001?nbOfProducts=1")
    .then(res => {
      expect(res.text).toMatchSnapshot();
      done();
    });
});

test("handles unknown products gracefully", done => {
  request(app)
    .get("/similar-color-products/FAKEID?nbOfProducts=1")
    .then(res => {
      expect(res.text).toMatchSnapshot();
      done();
    });
});
