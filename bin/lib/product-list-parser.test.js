const { Readable } = require("stream");
const parseProductList = require("./product-list-parser");

test("it should parse products in csv", async () => {
  const csv = `id;title;gender_id;composition;sleeve;photo;url
L1212-00-001;Polo Lacoste L.12.12 uni;MAN;100% Coton;Manches courtes;//image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/default/L1212_001_24.jpg?sw=458&sh=443;https://www.lacoste.com/fr/lacoste/homme/vetements/polos/polo-lacoste-l.12.12-uni/L1212-00.html?dwvar_L1212-00_color=001
L1212-00-031;Polo Lacoste L.12.12 uni;MAN;100% Coton;Manches courtes;//image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/default/L1212_031_24.jpg?sw=458&sh=443;https://www.lacoste.com/fr/lacoste/homme/vetements/polos/polo-lacoste-l.12.12-uni/L1212-00.html?dwvar_L1212-00_color=031`;

  const csvStream = new Readable();
  csvStream.push(csv);
  csvStream.push(null);

  const res = await parseProductList(csvStream);
  expect(res).toEqual([
    {
      id: "L1212-00-001",
      title: "Polo Lacoste L.12.12 uni",
      gender_id: "MAN",
      composition: "100% Coton",
      sleeve: "Manches courtes",
      photo:
        "//image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/default/L1212_001_24.jpg?sw=458&sh=443",
      url:
        "https://www.lacoste.com/fr/lacoste/homme/vetements/polos/polo-lacoste-l.12.12-uni/L1212-00.html?dwvar_L1212-00_color=001"
    },
    {
      id: "L1212-00-031",
      title: "Polo Lacoste L.12.12 uni",
      gender_id: "MAN",
      composition: "100% Coton",
      sleeve: "Manches courtes",
      photo:
        "//image1.lacoste.com/dw/image/v2/AAQM_PRD/on/demandware.static/Sites-FR-Site/Sites-master/default/L1212_031_24.jpg?sw=458&sh=443",
      url:
        "https://www.lacoste.com/fr/lacoste/homme/vetements/polos/polo-lacoste-l.12.12-uni/L1212-00.html?dwvar_L1212-00_color=031"
    }
  ]);
});
