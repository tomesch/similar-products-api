const { getHighestScoringColor } = require("./image-dominant-color.js");

test("it should get the highest scoring color", async () => {
  const imageProperties = [
    {
      faceAnnotations: [],
      landmarkAnnotations: [],
      logoAnnotations: [],
      labelAnnotations: [],
      textAnnotations: [],
      safeSearchAnnotation: null,
      imagePropertiesAnnotation: {
        dominantColors: {
          colors: [
            {
              color: { red: 246, green: 9, blue: 104, alpha: null },
              score: 0.11021572351455688,
              pixelFraction: 0.02172222174704075
            },
            {
              color: { red: 223, green: 1, blue: 27, alpha: null },
              score: 0.0850924402475357,
              pixelFraction: 0.017444444820284843
            },
            {
              color: { red: 74, green: 133, blue: 62, alpha: null },
              score: 0.02957439236342907,
              pixelFraction: 0.08150000125169754
            },
            {
              color: { red: 84, green: 163, blue: 112, alpha: null },
              score: 0.01981140859425068,
              pixelFraction: 0.023666666820645332
            },
            {
              color: { red: 172, green: 202, blue: 156, alpha: null },
              score: 0.011244365014135838,
              pixelFraction: 0.011500000022351742
            },
            {
              color: { red: 120, green: 203, blue: 87, alpha: null },
              score: 0.006709256675094366,
              pixelFraction: 0.004999999888241291
            },
            {
              color: { red: 29, green: 54, blue: 30, alpha: null },
              score: 0.002897696103900671,
              pixelFraction: 0.061444442719221115
            },
            {
              color: { red: 201, green: 196, blue: 189, alpha: null },
              score: 0.0022888791281729937,
              pixelFraction: 0.04488888755440712
            },
            {
              color: { red: 218, green: 9, blue: 87, alpha: null },
              score: 0.09860069304704666,
              pixelFraction: 0.02522222138941288
            },
            {
              color: { red: 222, green: 2, blue: 60, alpha: null },
              score: 0.08393125236034393,
              pixelFraction: 0.02983333356678486
            }
          ]
        }
      },
      error: null,
      cropHintsAnnotation: {
        cropHints: [
          {
            boundingPoly: {
              vertices: [
                { x: 0, y: 0 },
                { x: 2370, y: 0 },
                { x: 2370, y: 1896 },
                { x: 0, y: 1896 }
              ]
            },
            confidence: 0.7999999523162842,
            importanceFraction: 1
          }
        ]
      },
      fullTextAnnotation: null,
      webDetection: null
    }
  ];

  expect(getHighestScoringColor(imageProperties)).toEqual({
    alpha: null,
    blue: 104,
    green: 9,
    red: 246
  });
});
