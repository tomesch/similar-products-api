const Color = require("color");
const vision = require("@google-cloud/vision");

const gcVisionClient = new vision.ImageAnnotatorClient();

const RED_LUMINANCE = 0.2126;
const GREEN_LUMINANCE = 0.7152;
const BLUE_LUMINANCE = 0.0722;

module.exports.getHighestScoringColor = getHighestScoringColor = imageProperties => {
  return imageProperties[0].imagePropertiesAnnotation.dominantColors.colors.sort(
    (a, b) => b.score - a.score
  )[0].color;
};

module.exports.getDominantColor = async imageUrl => {
  try {
    const imageProperties = await gcVisionClient.imageProperties(imageUrl);

    const { red, green, blue } = getHighestScoringColor(imageProperties);

    return {
      lum:
        RED_LUMINANCE * red + GREEN_LUMINANCE * green + BLUE_LUMINANCE * blue,
      rgb: { r: red, g: green, b: blue },
      hsv: Color.rgb(red, green, blue)
        .hsv()
        .object()
    };
  } catch (err) {
    throw err;
  }
};
