const fs = require("fs");
const parse = require("csv-parse");

module.exports = async csvStream => {
  return new Promise((resolve, reject) => {
    const products = [];

    csvStream
      .pipe(parse({ columns: true, delimiter: ";" }))
      .on("data", products.push.bind(products))
      .on("finish", () => {
        resolve(products);
      })
      .on("error", reject);
  });
};
