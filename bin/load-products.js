#!/usr/bin/env node

const fs = require("fs");
const parse = require("csv-parse");
const yargs = require("yargs");
const parseProductList = require("./lib/product-list-parser");

const argv = yargs
  .usage("Usage: $0 --input [input.csv] --output [output.json]")
  .option("input", {
    alias: "i",
    describe: "provide the csv product list file path",
    demandOption: true
  })
  .option("output", {
    alias: "o",
    describe: "provide the json output file path",
    demandOption: true
  })
  .help().argv;

(async () => {
  const products = await parseProductList(fs.createReadStream(argv.input));
  fs.writeFile(argv.output, JSON.stringify(products), err => {
    if (err) {
      console.error(err);
      return;
    }

    console.log(`Loaded ${products.length} products in "${argv.output}"`);
  });
})();
