#!/usr/bin/env node

const fs = require("fs");
const parse = require("csv-parse");
const yargs = require("yargs");
const { getDominantColor } = require("./lib/image-dominant-color");

const argv = yargs
  .usage("Usage: $0 --input [input.csv] --output [output.json]")
  .option("input", {
    alias: "i",
    describe: "provide the json product list file path",
    demandOption: true
  })
  .option("output", {
    alias: "o",
    describe: "provide the json output file path",
    demandOption: true
  })
  .help().argv;

fs.readFile(argv.input, "utf8", async (err, products) => {
  if (err) {
    throw err;
  }

  try {
    products = JSON.parse(products);
  } catch (err) {
    throw err;
  }

  await products.reduce(async (res, product) => {
    await res;
    console.log(
      `Fetching dominant color for product "${product.id}" (https:${
        product.photo
      })`
    );
    try {
      const dominantColor = await getDominantColor("https:" + product.photo);
      product.color = dominantColor;
    } catch (err) {
      console.error(err);
    }
  }, Promise.resolve());

  fs.writeFile(argv.output, JSON.stringify(products), err => {
    if (err) {
      throw err;
    }

    console.log(`Fetched dominant color for ${products.length} products`);
  });
});
