# similar-products-api

## Quickstart

### Loading products

Load a CSV formated product list into a JSON file:

```
  node bin/load-products.js --input [input.csv] --output [output.json]
```

### Enriching the product database with color informations

Enrich the previously generated JSON product database with a dominant color property:

```
  GOOGLE_APPLICATION_CREDENTIALS=[path to your service account key] node bin/set-products-dominant-color-property.js --input [input.json] --output [output.json]
```

### Starting the API server

```
  PORT=3000 PRODUCT_DATABASE_PATH=[path to your JSON product database file] npm start
```

If the PRODUCT_DATABASE_PATH environment variable isn't set, the server will load the default database found in db/products-with-color.json.

## Endpoints

### GET /similar-color-products/:productId[?nbOfProducts=[int]&]format=[json | html]]

Return 3 (or nbOfProducts) similarly colored products. The endpoints supports 2 output formats (using the format query parameter) HTML for quick visual testing and JSON.
