const path = require("path");

module.exports = {
  PORT: process.env.PORT || 3000,
  PRODUCT_DATABASE_PATH:
    process.env.PRODUCT_DATABASE_PATH ||
    path.resolve(__dirname, "../../", "db/products-with-color.json")
};
