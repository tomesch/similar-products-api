const { Router } = require("express");
const similarColorProductsRouter = require("./similar-color-products");

module.exports = router = Router();

router.use("/similar-color-products", similarColorProductsRouter);
