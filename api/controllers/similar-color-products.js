const { Router } = require("express");
const { getSimilarColorProducts } = require("../models/products");

module.exports = router = Router();

router.get("/:productId", async (req, res) => {
  const format = req.query.format === "html" ? "html" : "json";
  const nbOfProducts = parseInt(req.query.nbOfProducts) || 3;

  try {
    const similarProducts = await getSimilarColorProducts(
      req.params.productId,
      nbOfProducts
    );

    if (format === "json") {
      res.json(similarProducts);
    } else {
      res.send(
        similarProducts
          .map(product => `<img size="250" src="https:${product.photo}" />`)
          .join("")
      );
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});
