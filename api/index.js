const express = require("express");
const pkg = require("../package.json");
const CONFIG = require("./config");
const routers = require("./controllers");

module.exports = app = express();

app.use("/", routers);

app.listen(CONFIG.PORT, function(e) {
  console.log(`${pkg.name} listening on port ${this.address().port}`);
});
