const rewire = require("rewire");

const productsModel = rewire("./products.js");
const sortByColor = productsModel.__get__("sortByColor");
const getSimilarColorProducts = productsModel.__get__(
  "getSimilarColorProducts"
);

const PRODUCTS = [
  {
    id: 3,
    color: {
      lum: 234,
      rgb: { r: 234, g: 234, b: 234 },
      hsv: { h: 240, s: 0, v: 14 }
    }
  },
  {
    id: 2,
    color: {
      lum: 234,
      rgb: { r: 33, g: 33, b: 35 },
      hsv: { h: 240, s: 5.7142857142857135, v: 13 }
    }
  },
  {
    id: 0,
    color: {
      lum: 69.5298,
      rgb: { r: 51, g: 76, b: 60 },
      hsv: { h: 141.60000000000002, s: 32.89473684210527, v: 29.80392156862745 }
    }
  },
  {
    id: 1,
    color: {
      lum: 37.8772,
      rgb: { r: 36, g: 36, b: 62 },
      hsv: { h: 240, s: 41.935483870967744, v: 24.313725490196077 }
    }
  },
  {
    id: 5,
    color: {
      lum: 51.2342,
      rgb: { r: 180, g: 15, b: 31 },
      hsv: { h: 354.18181818181824, s: 91.66666666666666, v: 70.58823529411765 }
    }
  },
  {
    id: 4,
    color: {
      lum: 41.7064,
      rgb: { r: 115, g: 21, b: 31 },
      hsv: { h: 353.6170212765958, s: 81.73913043478261, v: 45.09803921568628 }
    }
  }
];
const SORTED_PRODUCTS = [
  PRODUCTS[2],
  PRODUCTS[3],
  PRODUCTS[1],
  PRODUCTS[0],
  PRODUCTS[5],
  PRODUCTS[4]
];

test("sorts products by color", () => {
  expect([...PRODUCTS].sort(sortByColor)).toEqual([
    PRODUCTS[2],
    PRODUCTS[3],
    PRODUCTS[1],
    PRODUCTS[0],
    PRODUCTS[5],
    PRODUCTS[4]
  ]);
});

test("returns similarly colored products", async () => {
  const revert = productsModel.__set__("PRODUCTS", SORTED_PRODUCTS);

  expect(await getSimilarColorProducts(1, 2)).toEqual([
    SORTED_PRODUCTS[0],
    SORTED_PRODUCTS[2]
  ]);

  revert();
});

test("hanldes unknown ids", async () => {
  const revert = productsModel.__set__("PRODUCTS", SORTED_PRODUCTS);

  expect(await getSimilarColorProducts("fakeID", 2)).toEqual([]);

  revert();
});

test("hanldes requests for an odd numbers of similar products", async () => {
  const revert = productsModel.__set__("PRODUCTS", SORTED_PRODUCTS);

  expect(await getSimilarColorProducts(1, 3)).toEqual([
    SORTED_PRODUCTS[0],
    SORTED_PRODUCTS[2],
    SORTED_PRODUCTS[3]
  ]);

  revert();
});

test("handles index edge cases", async () => {
  const revert = productsModel.__set__("PRODUCTS", SORTED_PRODUCTS);

  expect(await getSimilarColorProducts(0, 5)).toEqual([
    SORTED_PRODUCTS[1],
    SORTED_PRODUCTS[2],
    SORTED_PRODUCTS[3],
    SORTED_PRODUCTS[4],
    SORTED_PRODUCTS[5]
  ]);

  expect(await getSimilarColorProducts(1, 5)).toEqual([
    SORTED_PRODUCTS[0],
    SORTED_PRODUCTS[2],
    SORTED_PRODUCTS[3],
    SORTED_PRODUCTS[4],
    SORTED_PRODUCTS[5]
  ]);

  expect(await getSimilarColorProducts(1, 10)).toEqual([
    SORTED_PRODUCTS[0],
    SORTED_PRODUCTS[2],
    SORTED_PRODUCTS[3],
    SORTED_PRODUCTS[4],
    SORTED_PRODUCTS[5]
  ]);

  revert();
});
