const fs = require("fs");
const CONFIG = require("../config");

let PRODUCTS;

const sortByColor = (a, b) =>
  a.color.hsv.h - b.color.hsv.h ||
  a.color.lum - b.color.lum ||
  a.color.hsv.v - b.color.hsv.v;

const loadProducts = async () => {
  return new Promise((resolve, reject) => {
    fs.readFile(CONFIG.PRODUCT_DATABASE_PATH, "utf8", (err, products) => {
      if (err) {
        return reject(
          Error(
            `Failed to load product database "${CONFIG.PRODUCT_DATABASE_PATH}"`
          )
        );
      }

      // Sorting the products by color luminance, hue and value to make lookup by similar color faster
      // this should give satisfactory results overall but could fail with some products, particularly those with white-like colors
      resolve(JSON.parse(products).sort(sortByColor));
    });
  });
};

module.exports.getSimilarColorProducts = getSimilarColorProducts = async (
  productId,
  nbOfProducts = 5
) => {
  if (!PRODUCTS) {
    PRODUCTS = await loadProducts();
  }

  nbOfProducts = Math.min(nbOfProducts, PRODUCTS.length - 1);

  const productIndex = PRODUCTS.findIndex(product => product.id === productId);
  if (productIndex < 0) {
    return [];
  }

  // Given the products are sorted by color, we only have to return the nbOfProducts closest elements
  const similarProducts = [];
  let i = 0;
  while (similarProducts.length < nbOfProducts) {
    if (PRODUCTS[productIndex - i - 1]) {
      similarProducts.push(PRODUCTS[productIndex - i - 1]);
    }

    if (PRODUCTS[productIndex + i + 1]) {
      similarProducts.push(PRODUCTS[productIndex + i + 1]);
    }

    i++;
  }

  return similarProducts.slice(0, nbOfProducts);
};
